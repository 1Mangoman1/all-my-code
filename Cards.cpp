#include "Cards.hpp"

#include <iostream>
using namespace std;

void Card::Setup( string newRank, char newSuit)
{
    m_rank = newRank;
    m_suit = newSuit;
}
void Card::Display()
{
    cout << m_rank << "-" << m_suit;
}
void Card::DisplayName()
{
    if (m_rank == 'A') { cout << "Ace"; }
    else if (m_rank == 'J') { cout << "Jack"; }
    else if (m_rank == 'Q') { cout << "Queen"; }
    else if (m_rank == 'K') { cout << "King"; }
    else { cout << m_rank; }

    cout << " of ";

    if (m_suit == 'D') { cout << "Diamonds"; }
    else if (m_suit == 'H') { cout << "Hearts"; }
    else if (m_suit == 'S') { cout << "Spades"; }
    else if (m_suit == 'C') { cout << "Clubs"; }
}

string Card::GetRank()
{
    return m_rank;
}
char Card::GetSuit()
{
    return m_suit;
}
