#include <iostream>
using namespace std;

#include "Die.hpp"

#include <ctime>

main()

srand( time( NULL) );

Die d20( 20 );
Die d8( 8 );

cout << "Roll d20: " << d20.Roll() << endl;
cout << "Roll d8: " << d8.Roll() << endl;

return 0;
