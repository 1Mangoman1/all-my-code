#include <iostream>
#include <string>
using namespace std;

int main()
{
    //base stuff (1)
    int hunger = 0;
    int health = 100;
    int happiness = 100;
    string petName;
    cout << "Please enter your pet's name: ";
    cin >> petName;
    // booleans (2)
    bool isDone = false;
    int menuChoice;
    while (!isDone)

    {
        cout << "\t Hunger: " << hunger << "%" << "\t Health: " << health << "%" << "\t Happiness: " << happiness << "%" << endl;
        cout << "_______________________________________________________" << endl;
        cout << "OPTIONS: 1. Feed.    2. Play.    3. Vet.    4. Quit." << endl;
        cout << "> ";
        cin >> menuChoice;

        hunger = hunger + 5;
        if (hunger >= 50)
        {
            happiness = happiness - 10;
            health = health - 10;
        }
        else
        {
            happiness = happiness - 5;
        }

        // change for hunger
        if (hunger > 100)
        {
            hunger == 100;
        }
        if (hunger < 0)
        {
            hunger = 0;
        }
        //change for health
        if (health < 0)
        {
            health = 0;
            isDone = true;
            cout << "You haven't taken care of " << petName << "." << endl;
            cout << petName << " has been removed from your care.";
        }
        if (health > 100)
        {
            health = 100;
        }
        //change for happiness
        if (happiness < 0)
        {
            happiness = 0;
        }
        if (happiness > 100)
        {
            happiness = 100;
        }
        int foodChoice;
        if (menuChoice == 1)
        {
            cout << "1. Pizza.    2. Broccoli.    3. Tuna. " << endl;
            cout << "> ";
            cin >> foodChoice;
            if (foodChoice == 1)
            {
                hunger = hunger - 15;
                health = health -1;
            }
            if (foodChoice == 2)
            {
                hunger = hunger - 10;
                health = health + 1;
            }
            if (foodChoice == 3)
            {
                hunger = hunger -12;
                health = health + 2;
            }
        }

        int playChoice;
        if (menuChoice == 2)
        {
            cout << "1. Fetch.    2. Tug-O-War.    3. Videogames." << endl;
            cout << "> ";
            cin >> playChoice;
            if (playChoice == 1)
            {
                happiness = happiness + 8;
                health = health + 2;
            }
            if (playChoice == 2)
            {
                happiness = happiness + 9;
            }
            if (playChoice == 3)
            {
                happiness = happiness + 10;
                health = health - 1;
            }
        }
        if (menuChoice == 3)
        {
            if (happiness <= 50)
            {
                cout << "Make sure you play with " << petName << " more." << endl;
            }
            if (hunger >= 50)
            {
                cout << "Make sure to feed " << petName << endl;
            }
            if (health <= 50)
            {
                cout << petName << " isn't looking too healthy. Take better care of it!" << endl;
            }
            if (happiness > 50 && hunger < 50 && health > 50)
            {
                cout << petName << " is Okay!" << endl;
            }
        }
        int choice;
        if (menuChoice == 4)
        {
            cout << "Are you sure you want to quit?" << endl;
            cout << "1. QUIT.    2. Don't quit." << endl;
            cout << "> ";
            cin >> choice;
        }
        if (choice == 1)
        {
            isDone = true;
        }
        if (choice == 2)
        {
            isDone = false;
        }
    }
}
